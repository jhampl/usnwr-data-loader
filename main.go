package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strings"
)

func main() {
	const INPUT_FILE = "H8 Upload Test.csv" //"H8 Upload Test.csv" //TODO: Command-line arg
	const OUTPUT_FILE = "output.csv"        //TODO: Command-line arg

	// Open source file and read as a CSV
	data, err := readInputFile(INPUT_FILE)
	if err != nil {
		log.Fatal(err)
	}

	//Split the header from the records
	columnHeaders, records := splitCSV(data)

	if !columnHeaders.contains("MRN") {
		log.Fatal("CSV must contain column titled MRN")
	}

	// Open output file for CSV writing
	outputFile, err := os.Create(OUTPUT_FILE)
	if err != nil {
		log.Fatal(err)
	}
	w := csv.NewWriter(outputFile)

	var loadableRecord []string

	for _, record := range records {
		loadableString := prepareLoadableString(columnHeaders, record)
		loadableRecord = append(loadableRecord, loadableString)
		err := w.Write(loadableRecord) //TODO: Write the full loadableRecord to buffer
		if err != nil {
			log.Fatal("Fatal error writing to buffer")
		}
		fmt.Println(loadableString)
		loadableRecord = nil
	}
	w.Flush()
}

type csvHeaders []string

func (h *csvHeaders) contains(element string) bool {
	for _, val := range *h {
		if val == element {
			return true
		}
	}
	return false
}

func readInputFile(path string) (data [][]string, err error) {
	file, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	csvReader := csv.NewReader(file)
	csvReader.TrimLeadingSpace = true

	readData, err := csvReader.ReadAll()
	return readData, err
}

func splitCSV(csv [][]string) (columnHeaders csvHeaders, records [][]string) {
	return csv[0], csv[1:]
}

func prepareLoadableString(columnHeaders []string, record []string) (s string) {
	const targetKeyValuePairDelimiter = "="
	const targetRecordDelimiter = "^"
	var loadableString string
	var keyValuePair string
	for columnIndex := range columnHeaders {
		keyValuePair = columnHeaders[columnIndex] + targetKeyValuePairDelimiter + record[columnIndex]
		loadableString = loadableString + keyValuePair + targetRecordDelimiter
	}
	loadableString = strings.TrimSuffix(loadableString, targetRecordDelimiter)
	return loadableString
}
